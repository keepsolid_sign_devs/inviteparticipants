#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlComponent>

//#include "InviteController.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

//    QQmlComponent component(&engine);
//    component.loadUrl(QUrl(QLatin1String("qrc:/InviteDialog.qml")));
//    QObject *window = component.create(engine.rootContext());

//    InviteController inviteController(qobject_cast<QQuickWindow*>(window));
//    inviteController.showDialog();

    return app.exec();
}
