import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

ApplicationWindow {
    id: appWindow
    visible: true
    width: 400
    height: 480
    title: qsTr("Main")

    header: ToolBar {
        RowLayout {
            ToolButton {
                text: qsTr("+")
                onClicked: inviteWindow.show()
            }
        }
    }

    InviteWindow {
        id: inviteWindow
    }
}

