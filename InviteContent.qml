import QtQuick 2.9
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQml.Models 2.2

Item {
    id: inviteContent
    property int invited: 0

    ColumnLayout {
        anchors.fill: parent

        TabBar {
            id: sourceSelector
            anchors {
                left: parent.left; right: parent.right
            }
            currentIndex: 0
            font.capitalization: Font.MixedCase
            TabButton {
                text: qsTr("Contacts")
            }
            TabButton {
                text: qsTr("Address book")
            }
        }

        Rectangle {
            id: searchField
            anchors {
                left: parent.left; right: parent.right
            }
            height: searchEdit.height

            TextField {
                id: searchEdit
                anchors {
                    left: parent.left; right: searchButton.visible? searchButton.left : parent.right
                }
                placeholderText: qsTr("Search contacts...")
                clip: true
            }

            AbstractButton {
                id: searchButton
                width: height
                height: parent.height - 10
                anchors { right: parent.right; rightMargin: 5; verticalCenter: parent.verticalCenter }

                Image {
                    id: searchIcon
                    width: parent.height
                    height: parent.height
                    anchors { verticalCenter: parent.verticalCenter; horizontalCenter: parent.horizontalCenter }
                    source: "qrc:/icons/simple_search_128.png"
                }

                onClicked: {
                    console.log("search action")
                }
            }
        }

        ListView {
            id: participantsView
            anchors {
                left: parent.left; right: parent.right; bottom: parent.bottom
            }
            Layout.fillHeight: true
            model: multiselectModel
            section.property: "name"
            section.criteria: ViewSection.FirstCharacter
            section.delegate: Text {
                text: section
                color: "dodgerblue"
            }
            currentIndex: -1
            clip: true
            ScrollBar.vertical: ScrollBar{ id: verticalScroll; policy: ScrollBar.AsNeeded }
        }
    }
//    ItemSelectionModel
    DelegateModel {
        id: multiselectModel
        model: ListModel {
            id: participantsList
            ListElement { name: "Daniel Craig";     mail: "daniel2006@bond007.uk" }
            ListElement { name: "David Niven";      mail: "david1967@bond007.uk" }
            ListElement { name: "George Lazenby";   mail: "george1969@bond007.uk" }
            ListElement { name: "Pierce Brosnan";   mail: "pierce1995@bond007.uk" }
            ListElement { name: "Roger Moore";      mail: "roger1973@bond007.uk" }
            ListElement { name: "Sean Connery";     mail: "sean1962@bond007.uk" }
            ListElement { name: "Timothy Dalton";   mail: "timothy1987@bond007.uk" }
        }
        groups: [
            DelegateModelGroup {
                id: selectedItems;
                name: "selected"
            }
        ]
        delegate: Rectangle {
            id: item
            clip: true
            width: parent.width - verticalScroll.width
            height: cell.height+2
            color: item.DelegateModel.inSelected? "lightskyblue" : "white"
            Column {
                id: cell
                Text { text: name }
                Text { text: mail }
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    item.DelegateModel.inSelected = !item.DelegateModel.inSelected;
                    participantsView.currentIndex = index;
                    inviteContent.invited = selectedItems.count
                }
            }
        }
    }
}

