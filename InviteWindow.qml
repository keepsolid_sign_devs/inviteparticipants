import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

Window {
    id: inviteWindow
    objectName: "InviteWindow"
    width: 310
    height: 410
    minimumWidth: 250
    minimumHeight: 150
    title: qsTr("Add participants")
    modality: Qt.ApplicationModal
    flags: Qt.Dialog | Qt.WindowTitleHint | Qt.WindowCloseButtonHint | Qt.CustomizeWindowHint

    Shortcut {
        sequence: "Esc"
        enabled: true
        onActivated: {
            inviteWindow.close()
        }
    }

    data: ColumnLayout {
        anchors {
            fill: parent; margins: 5//leftMargin: 5; rightMargin: 5
        }
        InviteContent {
            id: inviteContent
            anchors {
                fill: parent; bottomMargin: bottomRow.height
            }
        }
        RowLayout {
            id: bottomRow
            anchors {
                left: parent.left; right: parent.right; bottom: parent.bottom
            }
            Label {
                id: choosenLabel
                anchors.left: bottomRow.left;
                text: qsTr("Choosen: ")
            }
            Label {
                id: choosenNum
                property int value: inviteContent.invited
                anchors.left: choosenLabel.right;
                Layout.fillWidth: true
                text: value
            }
            Button {
                id: applyButton
                highlighted: inviteContent.invited > 0
                anchors.right: bottomRow.right;
                onClicked: inviteWindow.close()
                text: qsTr("Apply")
                font.capitalization: Font.MixedCase
            }
            DropShadow {
                anchors.fill: applyButton
                source: applyButton
                color: inviteContent.invited > 0? "#992f82c9" : "lightgray"
                transparentBorder: true
                horizontalOffset: 3
                verticalOffset: 3
                radius: 3
            }
        }
    }
}
